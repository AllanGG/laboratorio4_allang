/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;
import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import negocio.Usuarios;
/**
 *
 * @author allan
 */
public class frmBoletos extends javax.swing.JFrame {

    int intPago;
    int campos = 0;
    String matriz[][] = new String[9][8];
    String letras[] = {"A","B","C","D","E","F","G","H","I"};
    String numero[] = {"8","7","6","5","4","3","2","1"};
    ArrayList seleccionados = new ArrayList();
    
    Usuarios personas = new Usuarios();
    
    
    public frmBoletos(String cedula) {
        initComponents();
        
        setLocationRelativeTo(null);
      
        String strCedula = cedula;
        
        Calendar cal=Calendar.getInstance();
        String fecha=cal.get(cal.DATE)+"/"+cal.get(cal.MONTH)+"/"+cal.get(cal.YEAR);
        String hora=cal.get(cal.HOUR_OF_DAY)+":"+cal.get(cal.MINUTE);
        lblFecha.setText(fecha+"      "+hora);
        llenarMatriz();
        
        
        
        /*
        if(seleccionados.equals("A1")){
            btn1A.setForeground(Color.red);
            btn1A.enable(false);
        }else{
        }
        */
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnConfirmar2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblElecciones = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btn5D = new javax.swing.JButton();
        btn8E = new javax.swing.JButton();
        btn7E = new javax.swing.JButton();
        btn6E = new javax.swing.JButton();
        btn5E = new javax.swing.JButton();
        btn8F = new javax.swing.JButton();
        btn7F = new javax.swing.JButton();
        btn6F = new javax.swing.JButton();
        btn8A = new javax.swing.JButton();
        btn7A = new javax.swing.JButton();
        btn6A = new javax.swing.JButton();
        btn5F = new javax.swing.JButton();
        btn5A = new javax.swing.JButton();
        btn8G = new javax.swing.JButton();
        btn7G = new javax.swing.JButton();
        btn6G = new javax.swing.JButton();
        btn5G = new javax.swing.JButton();
        btn8B = new javax.swing.JButton();
        btn7B = new javax.swing.JButton();
        btn6B = new javax.swing.JButton();
        btn5B = new javax.swing.JButton();
        btn8H = new javax.swing.JButton();
        btn7H = new javax.swing.JButton();
        btn6H = new javax.swing.JButton();
        btn8C = new javax.swing.JButton();
        btn7C = new javax.swing.JButton();
        btn6C = new javax.swing.JButton();
        btn5C = new javax.swing.JButton();
        btn8I = new javax.swing.JButton();
        btn7I = new javax.swing.JButton();
        btn6I = new javax.swing.JButton();
        btn5I = new javax.swing.JButton();
        btn8D = new javax.swing.JButton();
        btn7D = new javax.swing.JButton();
        btn5H = new javax.swing.JButton();
        btn6D = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btn1I = new javax.swing.JButton();
        btn4D = new javax.swing.JButton();
        btn3D = new javax.swing.JButton();
        btn2D = new javax.swing.JButton();
        btn1C = new javax.swing.JButton();
        btn4E = new javax.swing.JButton();
        btn3E = new javax.swing.JButton();
        btn2E = new javax.swing.JButton();
        btn1E = new javax.swing.JButton();
        btn4F = new javax.swing.JButton();
        btn3F = new javax.swing.JButton();
        btn2F = new javax.swing.JButton();
        btn1F = new javax.swing.JButton();
        btn4A = new javax.swing.JButton();
        btn3A = new javax.swing.JButton();
        btn2A = new javax.swing.JButton();
        btn1A = new javax.swing.JButton();
        btn4G = new javax.swing.JButton();
        btn4B = new javax.swing.JButton();
        btn3G = new javax.swing.JButton();
        btn2G = new javax.swing.JButton();
        btn1G = new javax.swing.JButton();
        btn3B = new javax.swing.JButton();
        btn2B = new javax.swing.JButton();
        btn1B = new javax.swing.JButton();
        btn4I = new javax.swing.JButton();
        btn1D = new javax.swing.JButton();
        btn2I = new javax.swing.JButton();
        btn4H = new javax.swing.JButton();
        btn3H = new javax.swing.JButton();
        btn4C = new javax.swing.JButton();
        btn3C = new javax.swing.JButton();
        btn1H = new javax.swing.JButton();
        btn2C = new javax.swing.JButton();
        btn3I = new javax.swing.JButton();
        btn2H = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblPago = new javax.swing.JLabel();
        btnAtras = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Comprar Boletos");
        setLocationByPlatform(true);

        btnConfirmar2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnConfirmar2.setText("Confirmar");
        btnConfirmar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmar2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Elecciones:");

        lblFecha.setText("fecha");

        jLabel6.setBackground(new java.awt.Color(0, 0, 0));
        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Pantalla");
        jLabel6.setToolTipText("");
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblElecciones.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblElecciones.setText("0 / 5");

        jPanel1.setEnabled(false);

        btn5D.setText("5D");
        btn5D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5DActionPerformed(evt);
            }
        });

        btn8E.setText("8E");
        btn8E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8EActionPerformed(evt);
            }
        });

        btn7E.setText("7E");
        btn7E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7EActionPerformed(evt);
            }
        });

        btn6E.setText("6E");
        btn6E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6EActionPerformed(evt);
            }
        });

        btn5E.setText("5E");
        btn5E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5EActionPerformed(evt);
            }
        });

        btn8F.setText("8F");
        btn8F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8FActionPerformed(evt);
            }
        });

        btn7F.setText("7F");
        btn7F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7FActionPerformed(evt);
            }
        });

        btn6F.setText("6F");
        btn6F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6FActionPerformed(evt);
            }
        });

        btn8A.setText("8A");
        btn8A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8AActionPerformed(evt);
            }
        });

        btn7A.setText("7A");
        btn7A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7AActionPerformed(evt);
            }
        });

        btn6A.setText("6A");
        btn6A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6AActionPerformed(evt);
            }
        });

        btn5F.setText("5F");
        btn5F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5FActionPerformed(evt);
            }
        });

        btn5A.setText("5A");
        btn5A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5AActionPerformed(evt);
            }
        });

        btn8G.setText("8G");
        btn8G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8GActionPerformed(evt);
            }
        });

        btn7G.setText("7G");
        btn7G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7GActionPerformed(evt);
            }
        });

        btn6G.setText("6G");
        btn6G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6GActionPerformed(evt);
            }
        });

        btn5G.setText("5G");
        btn5G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5GActionPerformed(evt);
            }
        });

        btn8B.setText("8B");
        btn8B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8BActionPerformed(evt);
            }
        });

        btn7B.setText("7B");
        btn7B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7BActionPerformed(evt);
            }
        });

        btn6B.setText("6B");
        btn6B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6BActionPerformed(evt);
            }
        });

        btn5B.setText("5B");
        btn5B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5BActionPerformed(evt);
            }
        });

        btn8H.setText("8H");
        btn8H.setToolTipText("");
        btn8H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8HActionPerformed(evt);
            }
        });

        btn7H.setText("7H");
        btn7H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7HActionPerformed(evt);
            }
        });

        btn6H.setText("6H");
        btn6H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6HActionPerformed(evt);
            }
        });

        btn8C.setText("8C");
        btn8C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8CActionPerformed(evt);
            }
        });

        btn7C.setText("7C");
        btn7C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7CActionPerformed(evt);
            }
        });

        btn6C.setText("6C");
        btn6C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6CActionPerformed(evt);
            }
        });

        btn5C.setText("5C");
        btn5C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5CActionPerformed(evt);
            }
        });

        btn8I.setText("8I");
        btn8I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8IActionPerformed(evt);
            }
        });

        btn7I.setText("7I");
        btn7I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7IActionPerformed(evt);
            }
        });

        btn6I.setText("6I");
        btn6I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6IActionPerformed(evt);
            }
        });

        btn5I.setText("5I");
        btn5I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5IActionPerformed(evt);
            }
        });

        btn8D.setText("8D");
        btn8D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8DActionPerformed(evt);
            }
        });

        btn7D.setText("7D");
        btn7D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7DActionPerformed(evt);
            }
        });

        btn5H.setText("5H");
        btn5H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5HActionPerformed(evt);
            }
        });

        btn6D.setText("6D");
        btn6D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6DActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("₡3000");

        btn1I.setText("1I");
        btn1I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1IActionPerformed(evt);
            }
        });

        btn4D.setText("4D");
        btn4D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4DActionPerformed(evt);
            }
        });

        btn3D.setText("3D");
        btn3D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3DActionPerformed(evt);
            }
        });

        btn2D.setText("2D");
        btn2D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2DActionPerformed(evt);
            }
        });

        btn1C.setText("1C");
        btn1C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1CActionPerformed(evt);
            }
        });

        btn4E.setText("4E");
        btn4E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4EActionPerformed(evt);
            }
        });

        btn3E.setText("3E");
        btn3E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3EActionPerformed(evt);
            }
        });

        btn2E.setText("2E");
        btn2E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2EActionPerformed(evt);
            }
        });

        btn1E.setText("1E");
        btn1E.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1EActionPerformed(evt);
            }
        });

        btn4F.setText("4F");
        btn4F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4FActionPerformed(evt);
            }
        });

        btn3F.setText("3F");
        btn3F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3FActionPerformed(evt);
            }
        });

        btn2F.setText("2F");
        btn2F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2FActionPerformed(evt);
            }
        });

        btn1F.setText("1F");
        btn1F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1FActionPerformed(evt);
            }
        });

        btn4A.setText("4A");
        btn4A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4AActionPerformed(evt);
            }
        });

        btn3A.setText("3A");
        btn3A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3AActionPerformed(evt);
            }
        });

        btn2A.setText("2A");
        btn2A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2AActionPerformed(evt);
            }
        });

        btn1A.setText("1A");
        btn1A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1AActionPerformed(evt);
            }
        });

        btn4G.setText("4G");
        btn4G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4GActionPerformed(evt);
            }
        });

        btn4B.setText("4B");
        btn4B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4BActionPerformed(evt);
            }
        });

        btn3G.setText("3G");
        btn3G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3GActionPerformed(evt);
            }
        });

        btn2G.setText("2G");
        btn2G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2GActionPerformed(evt);
            }
        });

        btn1G.setText("1G");
        btn1G.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1GActionPerformed(evt);
            }
        });

        btn3B.setText("3B");
        btn3B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3BActionPerformed(evt);
            }
        });

        btn2B.setText("2B");
        btn2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2BActionPerformed(evt);
            }
        });

        btn1B.setText("1B");
        btn1B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1BActionPerformed(evt);
            }
        });

        btn4I.setText("4I");
        btn4I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4IActionPerformed(evt);
            }
        });

        btn1D.setText("1D");
        btn1D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1DActionPerformed(evt);
            }
        });

        btn2I.setText("2I");
        btn2I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2IActionPerformed(evt);
            }
        });

        btn4H.setText("4H");
        btn4H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4HActionPerformed(evt);
            }
        });

        btn3H.setText("3H");
        btn3H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3HActionPerformed(evt);
            }
        });

        btn4C.setText("4C");
        btn4C.setToolTipText("");
        btn4C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4CActionPerformed(evt);
            }
        });

        btn3C.setText("3C");
        btn3C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3CActionPerformed(evt);
            }
        });

        btn1H.setText("1H");
        btn1H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1HActionPerformed(evt);
            }
        });

        btn2C.setText("2C");
        btn2C.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2CActionPerformed(evt);
            }
        });

        btn3I.setText("3I");
        btn3I.setToolTipText("");
        btn3I.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3IActionPerformed(evt);
            }
        });

        btn2H.setText("2H");
        btn2H.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2HActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("₡5000");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn2A, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn4A, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn3A, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn1A))
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn2B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn3B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn4B))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn4C, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn3C, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1C, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn2C, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn2D, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1D, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn3D, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn4D))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn2E, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1E, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn3E, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn4E))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btn4F, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn3F, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn2F, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1F, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn4G, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btn3G, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn2G, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn1G, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btn4H, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn2H, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1H, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn3H, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn4I, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                    .addComponent(btn3I, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn2I, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn1I, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn4A)
                            .addComponent(btn4B)
                            .addComponent(btn4C)
                            .addComponent(btn4D)
                            .addComponent(btn4E)
                            .addComponent(btn4F)
                            .addComponent(btn4G)
                            .addComponent(btn4H)
                            .addComponent(btn4I)))
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn3B)
                    .addComponent(btn3A)
                    .addComponent(btn3C)
                    .addComponent(btn3D)
                    .addComponent(btn3E)
                    .addComponent(btn3F)
                    .addComponent(btn3G)
                    .addComponent(btn3H)
                    .addComponent(btn3I, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn2A)
                    .addComponent(btn2B)
                    .addComponent(btn2C)
                    .addComponent(btn2D)
                    .addComponent(btn2E)
                    .addComponent(btn2F)
                    .addComponent(btn2G)
                    .addComponent(btn2H)
                    .addComponent(btn2I))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn1A)
                    .addComponent(btn1B)
                    .addComponent(btn1C)
                    .addComponent(btn1D)
                    .addComponent(btn1E)
                    .addComponent(btn1F)
                    .addComponent(btn1G)
                    .addComponent(btn1H)
                    .addComponent(btn1I))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn5A, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn7A, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn6A, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn8A, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn5B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn6B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn7B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn8B))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn8C, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn7C, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn6C, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn5C))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn5D, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn6D, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn7D, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn8D))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btn5E, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn6E, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn7E))
                            .addComponent(btn8E, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(btn5F, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn6F, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn7F, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(btn8F, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn5G, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(btn5H, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn5I, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btn8G, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn6G, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn7G, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn8H, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(btn7H, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btn6H, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btn6I, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btn7I, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(btn8I, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn8A)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn7A)
                        .addGap(7, 7, 7)
                        .addComponent(btn6A)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn5A))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn8B)
                            .addComponent(btn8C)
                            .addComponent(btn8D)
                            .addComponent(btn8F)
                            .addComponent(btn8G)
                            .addComponent(btn8H)
                            .addComponent(btn8I)
                            .addComponent(btn8E))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn7B)
                            .addComponent(btn7C)
                            .addComponent(btn7D)
                            .addComponent(btn7E)
                            .addComponent(btn7F)
                            .addComponent(btn7G)
                            .addComponent(btn7H)
                            .addComponent(btn7I))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn6B)
                            .addComponent(btn6C)
                            .addComponent(btn6D)
                            .addComponent(btn6E)
                            .addComponent(btn6F)
                            .addComponent(btn6G)
                            .addComponent(btn6H)
                            .addComponent(btn6I))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn5F)
                                .addComponent(btn5G)
                                .addComponent(btn5H)
                                .addComponent(btn5I))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn5B)
                                .addComponent(btn5C)
                                .addComponent(btn5D)
                                .addComponent(btn5E)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Total a pagar:");

        lblPago.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPago.setText("₡00000");

        btnAtras.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAtras.setText("Atrás");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(10, 10, 10)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnConfirmar2)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(lblElecciones)
                                            .addGap(203, 203, 203)
                                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(lblPago)))))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAtras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblFecha)
                        .addGap(67, 67, 67))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAtras)
                    .addComponent(lblFecha))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblPago)
                    .addComponent(jLabel1)
                    .addComponent(lblElecciones))
                .addGap(16, 16, 16)
                .addComponent(btnConfirmar2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public void llenarMatriz(){
        for(int conta=0;conta <=8;conta++){
            for(int conta2=0;conta2 <= 7;conta2++){
                matriz[conta][conta2] = ""+numero[conta2]+letras[conta];
                
                
            }
        }
        
    }
    
    private void btn1AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1AActionPerformed
        if(campos < 5){
            seleccionados.add("1A");
            btn1A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1AActionPerformed

    private void btn8FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8FActionPerformed
        if(campos < 5){
            seleccionados.add("8F");
            btn8F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8FActionPerformed

    private void btnConfirmar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmar2ActionPerformed
        
        
        /*try{
            File archivo = new File("ListaUsuarios.txt"); // este es el archivo que insertaras caracteres
            FileWriter escribir = new FileWriter(archivo);
            String texto = "aqui van los caracteres a insertar";
            for(int i=0; i<texto.length();i++){
                escribir.write(texto.charAt(i));
                escribir.close();
            }    
        }catch(Exception e){
            
        }*/
        
        
        
        
        frmMenu menu = new frmMenu();
        menu.setVisible(true);
        super.dispose();
        
        
        
        
        
        
        
    }//GEN-LAST:event_btnConfirmar2ActionPerformed

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        frmMenu menu = new frmMenu();
        menu.setVisible(true);
        super.dispose();
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btn2AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2AActionPerformed
        if(campos < 5){
            seleccionados.add("2A");
            btn2A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2AActionPerformed

    private void btn3AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3AActionPerformed
        if(campos < 5){
            seleccionados.add("3A");
            btn3A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3AActionPerformed

    private void btn4AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4AActionPerformed
        if(campos < 5){
            seleccionados.add("4A");
            btn4A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4AActionPerformed

    private void btn5AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5AActionPerformed
        if(campos < 5){
            seleccionados.add("5A");
            btn5A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5AActionPerformed

    private void btn6AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6AActionPerformed
        if(campos < 5){
            seleccionados.add("6A");
            btn6A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6AActionPerformed

    private void btn7AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7AActionPerformed
        if(campos < 5){
            seleccionados.add("7A");
            btn7A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7AActionPerformed

    private void btn8AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8AActionPerformed
        if(campos < 5){
            seleccionados.add("8A");
            btn8A.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8AActionPerformed

    private void btn1BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1BActionPerformed
        if(campos < 5){
            seleccionados.add("1B");
            btn1B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1BActionPerformed

    private void btn2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2BActionPerformed
        if(campos < 5){
            seleccionados.add("2B");
            btn2B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2BActionPerformed

    private void btn3BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3BActionPerformed
        if(campos < 5){
            seleccionados.add("3B");
            btn3B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3BActionPerformed

    private void btn4BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4BActionPerformed
        if(campos < 5){
            seleccionados.add("4B");
            btn4B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4BActionPerformed

    private void btn1CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1CActionPerformed
        if(campos < 5){
            seleccionados.add("1C");
            btn1C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1CActionPerformed

    private void btn2CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2CActionPerformed
        if(campos < 5){
            seleccionados.add("2C");
            btn2C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2CActionPerformed

    private void btn3CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3CActionPerformed
        if(campos < 5){
            seleccionados.add("3C");
            btn3C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3CActionPerformed

    private void btn4CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4CActionPerformed
        if(campos < 5){
            seleccionados.add("4C");
            btn4C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4CActionPerformed

    private void btn1DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1DActionPerformed
        if(campos < 5){
            seleccionados.add("1D");
            btn1D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1DActionPerformed

    private void btn2DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2DActionPerformed
        if(campos < 5){
            seleccionados.add("2D");
            btn2D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2DActionPerformed

    private void btn3DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3DActionPerformed
        if(campos < 5){
            seleccionados.add("3D");
            btn3D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3DActionPerformed

    private void btn4DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4DActionPerformed
        if(campos < 5){
            seleccionados.add("4D");
            btn4D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4DActionPerformed

    private void btn1EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1EActionPerformed
        if(campos < 5){
            seleccionados.add("1E");
            btn1E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1EActionPerformed

    private void btn2EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2EActionPerformed
        if(campos < 5){
            seleccionados.add("2E");
            btn2E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2EActionPerformed

    private void btn3EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3EActionPerformed
        if(campos < 5){
            seleccionados.add("3E");
            btn3E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3EActionPerformed

    private void btn4EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4EActionPerformed
        if(campos < 5){
            seleccionados.add("4E");
            btn4E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4EActionPerformed

    private void btn1FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1FActionPerformed
        if(campos < 5){
            seleccionados.add("1F");
            btn1F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1FActionPerformed

    private void btn2FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2FActionPerformed
        if(campos < 5){
            seleccionados.add("2F");
            btn2F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2FActionPerformed

    private void btn3FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3FActionPerformed
        if(campos < 5){
            seleccionados.add("3F");
            btn3F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3FActionPerformed

    private void btn4FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4FActionPerformed
        if(campos < 5){
            seleccionados.add("4F");
            btn4F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4FActionPerformed

    private void btn1GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1GActionPerformed
        if(campos < 5){
            seleccionados.add("1G");
            btn1G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1GActionPerformed

    private void btn2GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2GActionPerformed
        if(campos < 5){
            seleccionados.add("2G");
            btn2G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2GActionPerformed

    private void btn3GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3GActionPerformed
        if(campos < 5){
            seleccionados.add("3G");
            btn3G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3GActionPerformed

    private void btn4GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4GActionPerformed
        if(campos < 5){
            seleccionados.add("4G");
            btn4G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4GActionPerformed

    private void btn1HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1HActionPerformed
        if(campos < 5){
            seleccionados.add("1H");
            btn1H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1HActionPerformed

    private void btn2HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2HActionPerformed
        if(campos < 5){
            seleccionados.add("2H");
            btn2H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2HActionPerformed

    private void btn3HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3HActionPerformed
        if(campos < 5){
            seleccionados.add("3H");
            btn3H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3HActionPerformed

    private void btn4HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4HActionPerformed
        if(campos < 5){
            seleccionados.add("4H");
            btn4H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4HActionPerformed

    private void btn1IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1IActionPerformed
        if(campos < 5){
            seleccionados.add("1I");
            btn1I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn1IActionPerformed

    private void btn2IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2IActionPerformed
        if(campos < 5){
            seleccionados.add("2I");
            btn2I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn2IActionPerformed

    private void btn3IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3IActionPerformed
        if(campos < 5){
            seleccionados.add("3I");
            btn3I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn3IActionPerformed

    private void btn4IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4IActionPerformed
        if(campos < 5){
            seleccionados.add("4I");
            btn4I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+5000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn4IActionPerformed

    private void btn5BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5BActionPerformed
        if(campos < 5){
            seleccionados.add("5B");
            btn5B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5BActionPerformed

    private void btn6BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6BActionPerformed
        if(campos < 5){
            seleccionados.add("6B");
            btn6B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6BActionPerformed

    private void btn7BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7BActionPerformed
        if(campos < 5){
            seleccionados.add("7B");
            btn7B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7BActionPerformed

    private void btn8BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8BActionPerformed
        if(campos < 5){
            seleccionados.add("8B");
            btn8B.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8BActionPerformed

    private void btn5CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5CActionPerformed
        if(campos < 5){
            seleccionados.add("5C");
            btn5C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5CActionPerformed

    private void btn6CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6CActionPerformed
        if(campos < 5){
            seleccionados.add("6C");
            btn6C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6CActionPerformed

    private void btn7CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7CActionPerformed
        if(campos < 5){
            seleccionados.add("7C");
            btn7C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7CActionPerformed

    private void btn8CActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8CActionPerformed
        if(campos < 5){
            seleccionados.add("8C");
            btn8C.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8CActionPerformed

    private void btn5DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5DActionPerformed
        if(campos < 5){
            seleccionados.add("5D");
            btn5D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5DActionPerformed

    private void btn5EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5EActionPerformed
        if(campos < 5){
            seleccionados.add("5E");
            btn5E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5EActionPerformed

    private void btn6DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6DActionPerformed
        if(campos < 5){
            seleccionados.add("6D");
            btn6D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6DActionPerformed

    private void btn7DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7DActionPerformed
        if(campos < 5){
            seleccionados.add("7D");
            btn7D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7DActionPerformed

    private void btn8DActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8DActionPerformed
        if(campos < 5){
            seleccionados.add("8D");
            btn8D.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8DActionPerformed

    private void btn6EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6EActionPerformed
        if(campos < 5){
            seleccionados.add("6E");
            btn6E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6EActionPerformed

    private void btn7EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7EActionPerformed
        if(campos < 5){
            seleccionados.add("7E");
            btn7E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7EActionPerformed

    private void btn8EActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8EActionPerformed
        if(campos < 5){
            seleccionados.add("8E");
            btn8E.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8EActionPerformed

    private void btn5FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5FActionPerformed
        if(campos < 5){
            seleccionados.add("5F");
            btn5F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5FActionPerformed

    private void btn6FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6FActionPerformed
        if(campos < 5){
            seleccionados.add("6F");
            btn6F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6FActionPerformed

    private void btn7FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7FActionPerformed
        if(campos < 5){
            seleccionados.add("7F");
            btn7F.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7FActionPerformed

    private void btn5GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5GActionPerformed
        if(campos < 5){
            seleccionados.add("5G");
            btn5G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5GActionPerformed

    private void btn6GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6GActionPerformed
        if(campos < 5){
            seleccionados.add("6G");
            btn6G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6GActionPerformed

    private void btn7GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7GActionPerformed
        if(campos < 5){
            seleccionados.add("7G");
            btn7G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7GActionPerformed

    private void btn8GActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8GActionPerformed
        if(campos < 5){
            seleccionados.add("8G");
            btn8G.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8GActionPerformed

    private void btn5HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5HActionPerformed
        if(campos < 5){
            seleccionados.add("5H");
            btn5H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5HActionPerformed

    private void btn6HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6HActionPerformed
        if(campos < 5){
            seleccionados.add("6H");
            btn6H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6HActionPerformed

    private void btn7HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7HActionPerformed
        if(campos < 5){
            seleccionados.add("7H");
            btn7H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7HActionPerformed

    private void btn8HActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8HActionPerformed
        if(campos < 5){
            seleccionados.add("8H");
            btn8H.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8HActionPerformed

    private void btn5IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5IActionPerformed
        if(campos < 5){
            seleccionados.add("5I");
            btn5I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn5IActionPerformed

    private void btn6IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6IActionPerformed
        if(campos < 5){
            seleccionados.add("6I");
            btn6I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn6IActionPerformed

    private void btn7IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7IActionPerformed
        if(campos < 5){
            seleccionados.add("7I");
            btn7I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn7IActionPerformed

    private void btn8IActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8IActionPerformed
        if(campos < 5){
            seleccionados.add("8I");
            btn8I.setForeground(Color.green);
            campos=campos+1;
            lblElecciones.setText(campos+" / 5");   
            intPago=intPago+3000;
            lblPago.setText("₡"+intPago);
        }else{
            JOptionPane.showMessageDialog(null, "Campos maximos adqueridos","Advertencia", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn8IActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmBoletos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn1A;
    private javax.swing.JButton btn1B;
    private javax.swing.JButton btn1C;
    private javax.swing.JButton btn1D;
    private javax.swing.JButton btn1E;
    private javax.swing.JButton btn1F;
    private javax.swing.JButton btn1G;
    private javax.swing.JButton btn1H;
    private javax.swing.JButton btn1I;
    private javax.swing.JButton btn2A;
    private javax.swing.JButton btn2B;
    private javax.swing.JButton btn2C;
    private javax.swing.JButton btn2D;
    private javax.swing.JButton btn2E;
    private javax.swing.JButton btn2F;
    private javax.swing.JButton btn2G;
    private javax.swing.JButton btn2H;
    private javax.swing.JButton btn2I;
    private javax.swing.JButton btn3A;
    private javax.swing.JButton btn3B;
    private javax.swing.JButton btn3C;
    private javax.swing.JButton btn3D;
    private javax.swing.JButton btn3E;
    private javax.swing.JButton btn3F;
    private javax.swing.JButton btn3G;
    private javax.swing.JButton btn3H;
    private javax.swing.JButton btn3I;
    private javax.swing.JButton btn4A;
    private javax.swing.JButton btn4B;
    private javax.swing.JButton btn4C;
    private javax.swing.JButton btn4D;
    private javax.swing.JButton btn4E;
    private javax.swing.JButton btn4F;
    private javax.swing.JButton btn4G;
    private javax.swing.JButton btn4H;
    private javax.swing.JButton btn4I;
    private javax.swing.JButton btn5A;
    private javax.swing.JButton btn5B;
    private javax.swing.JButton btn5C;
    private javax.swing.JButton btn5D;
    private javax.swing.JButton btn5E;
    private javax.swing.JButton btn5F;
    private javax.swing.JButton btn5G;
    private javax.swing.JButton btn5H;
    private javax.swing.JButton btn5I;
    private javax.swing.JButton btn6A;
    private javax.swing.JButton btn6B;
    private javax.swing.JButton btn6C;
    private javax.swing.JButton btn6D;
    private javax.swing.JButton btn6E;
    private javax.swing.JButton btn6F;
    private javax.swing.JButton btn6G;
    private javax.swing.JButton btn6H;
    private javax.swing.JButton btn6I;
    private javax.swing.JButton btn7A;
    private javax.swing.JButton btn7B;
    private javax.swing.JButton btn7C;
    private javax.swing.JButton btn7D;
    private javax.swing.JButton btn7E;
    private javax.swing.JButton btn7F;
    private javax.swing.JButton btn7G;
    private javax.swing.JButton btn7H;
    private javax.swing.JButton btn7I;
    private javax.swing.JButton btn8A;
    private javax.swing.JButton btn8B;
    private javax.swing.JButton btn8C;
    private javax.swing.JButton btn8D;
    private javax.swing.JButton btn8E;
    private javax.swing.JButton btn8F;
    private javax.swing.JButton btn8G;
    private javax.swing.JButton btn8H;
    private javax.swing.JButton btn8I;
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnConfirmar2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblElecciones;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblPago;
    // End of variables declaration//GEN-END:variables
}
