/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.util.ArrayList;

/**
 *
 * @author allan
 */
public class objPersonas {
    private String strCedula;
    private String strNombre;
    private String strGenero;
    private String strAsientos;
    private Integer intPago;
    private String strFecha;
    
    
    public static ArrayList<objPersonas> ListaPersonas = new ArrayList<>();
    
    public objPersonas(String strCedula, String strNombre, String strGenero, String strAsientos, Integer intPago, String strFecha){
        this.strCedula = strCedula;
        this.strNombre = strNombre;
        this.strGenero = strGenero;
        this.strAsientos = strAsientos;
        this.intPago = intPago;
        this.strFecha = strFecha;
    }    

    public String getStrAsientos() {
        return strAsientos;
    }

    public void setStrAsientos(String strAsientos) {
        this.strAsientos = strAsientos;
    }

    public Integer getIntPago() {
        return intPago;
    }

    public void setIntPago(Integer intPago) {
        this.intPago = intPago;
    }

    public String getStrFecha() {
        return strFecha;
    }

    public void setStrFecha(String strFecha) {
        this.strFecha = strFecha;
    }

    public String getStrCedula() {
        return strCedula;
    }

    public void setStrCedula(String strCedula) {
        this.strCedula = strCedula;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public String getStrGenero() {
        return strGenero;
    }

    public void setStrGenero(String strGenero) {
        this.strGenero = strGenero;
    }
    
}
